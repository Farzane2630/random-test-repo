import { addArticle, removeArticle } from "./actionTypes.js";

const addArticleAction = (data) => {
  return { type: addArticle, payload: data };
};

const removeArticleAction = (data) => {
  return { type: removeArticle, id: id };
};

export { addArticleAction, removeArticleAction };
