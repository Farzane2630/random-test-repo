import productsReducer from "../Redux/Products/reducer.js";
import commentsReducer from "../Redux/Comments/reducer.js";
import articlesReducer from "../Redux/Articles/reducer.js";
import { addProductAction, removeProductAction } from "../Redux/Products/actionCreators.js";

const store = Redux.createStore(
  Redux.combineReducers({
    products: productsReducer,
    comments: commentsReducer,
    articles: articlesReducer,
  })
);

store.dispatch(addProductAction({
   id: 1,
   title: "iphone14",
   price: 80_000_000
}))
console.log(store.getState());

store.dispatch(removeProductAction(1))
console.log(store.getState());
